#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TEST_SUITE=${1:-0}
TEST_SUITE=$(printf "%02d" "$TEST_SUITE")
LIST_DIR="$DIR/list"
TEST_FILE="$LIST_DIR/tests-$TEST_SUITE.txt"
FAIL_FILE="$DIR/failed.txt"
FAILED=false

function checkout {
  if [ -d "gitlab-ee" ] ; then
    cd "gitlab-ee"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "https://gitlab.com/gitlab-org/gitlab-ee.git" "gitlab-ee"
    cd "gitlab-ee"
  fi
}

if [ ! -d "$LIST_DIR" ]; then
  curl --fail --location --silent --show-error \
    "$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_REF/download?job=list" \
    > list.zip
  unzip list.zip
fi

checkout
yarn

echo "running tests in $TEST_FILE"

rm -rf "$FAIL_FILE"
touch "$FAIL_FILE"

while read -r file; do
  if yarn karma -f "$file"; then
    echo "No side effects in $file"
  else
    FAILED=true
    echo "Tests in $file have side effects"
    echo "$file" >> "$FAIL_FILE"
  fi
done <"$TEST_FILE"

if [[ "$FAILED" == "true" ]]; then
  echo "The following files have side effects (or are currently failing on master):"
  cat "$FAIL_FILE"
  exit 1
fi
echo "No file had side effects (or is failing on master)"
exit 0