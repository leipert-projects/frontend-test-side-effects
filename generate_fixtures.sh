#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
RUNNERS=${RUNNERS:-12}

function checkout {
  if [ -d "gitlab-ee" ] ; then
    cd "gitlab-ee"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "https://gitlab.com/gitlab-org/gitlab-ee.git" "gitlab-ee"
    cd "gitlab-ee"
  fi
}

checkout
echo "Install dependencies"
bundle install -j4
echo "Copy config"
bash scripts/prepare_build.sh
echo "Create user"
bash scripts/create_postgres_user.sh
echo "Generate DB"
bundle exec rake db:drop db:create db:schema:load db:migrate
echo "Generate Webpack bundle"
bundle exec rake webpack:compile
echo "Generate Fixtures"
bundle exec rake karma:fixtures