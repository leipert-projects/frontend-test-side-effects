#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
RUNNERS=${RUNNERS:-12}

function checkout {
  if [ -d "gitlab-ee" ] ; then
    cd "gitlab-ee"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "https://gitlab.com/gitlab-org/gitlab-ee.git" "gitlab-ee"
    cd "gitlab-ee"
  fi
}

mkdir -p list
checkout
find * -iname '*_spec.js' | grep -v "node_modules" | sort | tee "$DIR/list/all.txt"
split --number="l/$RUNNERS" --numeric-suffixes --suffix-length=2 --additional-suffix ".txt" "$DIR/list/all.txt" "$DIR/list/tests-"
yarn